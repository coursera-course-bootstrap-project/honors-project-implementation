import "bootstrap"
import "./documentation.scss";

var myOffcanvas = document.getElementById('sidebar')
myOffcanvas.addEventListener('hide.bs.offcanvas', function () {
    console.log("hidden")

    var content = document.getElementById('content');
    if (!content.classList.contains('active')) {
        content.classList.add("active");
    }

    // var sidebar = document.getElementById('sidebar');
    // if (!sidebar.classList.contains('active')) {
    //     sidebar.classList.add("active");
    // }
});

myOffcanvas.addEventListener('show.bs.offcanvas', function () {
    console.log("show")
    var content = document.getElementById('content')
    if (content.classList.contains('active')) {
        content.classList.remove("active");
    }

    // var sidebar = document.getElementById('sidebar');
    // if (sidebar.classList.contains('active')) {
    //     sidebar.classList.remove("active");
    // }
});