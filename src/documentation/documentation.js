const bootstrap = require("bootstrap")
import "./documentation.scss";

var callback = function () {
    // Javascript to enable link to tab
    var hash = location.hash.replace(/^#/, '');
    if (hash) {
        var triggerableTab = document.querySelector('#Sidebar a[href="#' + hash + '"]');
        if (triggerableTab != undefined) {
            var tab = new bootstrap.Tab(triggerableTab);

            tab.show();
            window.scrollTo(0, 0);
        }
    }

    var tabEls = [].slice.call(document.querySelectorAll('a[data-bs-toggle="tab"]'));
    tabEls.forEach(function (tabEl) {
        tabEl.addEventListener('shown.bs.tab', function (event) {
            window.location.hash = event.target.hash
            setTimeout(function () {
                window.scrollTo(0, 0);
            },50);
        });
    });
};

if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
    callback();
} else {
    document.addEventListener("DOMContentLoaded", callback);
}

var sidebar = document.getElementById('Sidebar')
sidebar.addEventListener('hide.bs.offcanvas', function () {
    var content = document.getElementById('Content');
    if (!content.classList.contains('active')) {
        content.classList.add("active");
    }

    var sidebarController = document.getElementById('Sidebar-Controller');
    if (sidebarController.classList.contains('sidebar-open')) {
        sidebarController.classList.remove('sidebar-open')
        sidebarController.classList.add('sidebar-closed')
        sidebarController.innerHTML = "<i class=\"bi bi-chevron-bar-right\"></i>"
    }
});

sidebar.addEventListener('show.bs.offcanvas', function () {
    var content = document.getElementById('Content')
    if (content.classList.contains('active')) {
        content.classList.remove("active");
    }

    var sidebarController = document.getElementById('Sidebar-Controller');
    if (sidebarController.classList.contains('sidebar-closed')) {
        sidebarController.classList.remove('sidebar-closed')
        sidebarController.classList.add('sidebar-open')
        sidebarController.innerHTML = "<i class=\"bi bi-chevron-bar-left\"></i>"
    }
});