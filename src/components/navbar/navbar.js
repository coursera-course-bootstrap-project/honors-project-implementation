import "./navbar.scss";
import navbarExpanded from './navbar-expanded.html'
// import navbarCollapsed from './navbar-collapsed.html'

function addNavbar() {
    document.getElementById("Navbar-Expanded").innerHTML = navbarExpanded
    // document.getElementById("Navbar-Collapsed").innerHTML = navbarCollapsed
}

addNavbar()


var callback = function () {
    document.addEventListener('scroll', function(e) {
        if (window.scrollY < 100) {
            document.getElementById("Navbar").classList.remove("filled-nav")
        } else {
            document.getElementById("Navbar").classList.add("filled-nav")
        }
    });
};

if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
    callback();
} else {
    document.addEventListener("DOMContentLoaded", callback);
}