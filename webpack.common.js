const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
const { extendDefaultPlugins } = require("svgo");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: {
        navbar: './src/components/navbar/navbar.js',
        footer: './src/components/footer/footer.js',
        homepage: './src/homepage/homepage.js',
        nsm: './src/nitrado-server-manager/nitrado-server-manager.js',
        status_bot: './src/status-bot/status-bot.js',
        pricing: './src/pricing/pricing.js',
        contact: './src/contact/contact.js',
        documentation: `./src/documentation/documentation.js`,
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    // {
                    //     // inject CSS to page
                    //     loader: 'style-loader'
                    // }, 
                    {
                        // translates CSS into CommonJS modules
                        loader: 'css-loader'
                    }, 
                    {
                        // Run postcss actions
                        loader: 'postcss-loader',
                        options: {
                            // `postcssOptions` is needed for postcss 8.x;
                            // if you use postcss 7.x skip the key
                            postcssOptions: {
                                // postcss plugins, can be exported to postcss.config.js
                                plugins: function () {
                                    return [
                                    require('autoprefixer')
                                    ];
                                }
                            }
                        }
                    }, 
                    {
                        // compiles Sass to CSS
                        loader: 'sass-loader'
                    }
                ],
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.html$/i,
                loader: 'html-loader',
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                include: path.resolve(__dirname, './node_modules/bootstrap-icons/font/fonts'),
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'webfonts',
                        publicPath: '../webfonts',
                    },
                }
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/homepage/homepage.html',
            inject: true,
            chunks: ['homepage', 'navbar', 'footer'],
            filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/nitrado-server-manager/nitrado-server-manager.html',
            inject: true,
            chunks: ['nsm', 'navbar', 'footer'],
            filename: 'nitrado-server-manager.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/status-bot/status-bot.html',
            inject: true,
            chunks: ['status_bot', 'navbar', 'footer'],
            filename: 'status-bot.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/pricing/pricing.html',
            inject: true,
            chunks: ['pricing', 'navbar', 'footer'],
            filename: 'pricing.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/contact/contact.html',
            inject: true,
            chunks: ['contact', 'navbar', 'footer'],
            filename: 'support.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/documentation/documentation.html',
            inject: true,
            chunks: ['documentation', 'navbar'],
            filename: 'documentation.html'
        }),
        new ImageMinimizerPlugin({
            minimizerOptions: {
                // Lossless optimization with custom option
                // Feel free to experiment with options for better result for you
                plugins: [
                    ["gifsicle", { interlaced: true }],
                    ["jpegtran", { progressive: true }],
                    ["optipng", { optimizationLevel: 5 }],
                    // Svgo configuration here https://github.com/svg/svgo#configuration
                    // [
                    //     "svgo",
                    //     {
                    //         plugins: extendDefaultPlugins([
                    //             {
                    //                 name: "removeViewBox",
                    //                 active: false,
                    //             },
                    //             {
                    //                 name: "addAttributesToSVGElement",
                    //                 params: {
                    //                     attributes: [{ xmlns: "http://www.w3.org/2000/svg" }],
                    //                 },
                    //             },
                    //         ]),
                    //     },
                    // ],
                ],
            },
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
            chunkFilename: '[id].[contenthash].css',
        }),
    ],
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,
    },
    optimization: {
        moduleIds: 'deterministic',
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
    },
};